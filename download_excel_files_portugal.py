# -*- coding: utf-8 -*-

import selenium
import os
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from datetime import timedelta, date

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

start_date = date(2020, 9, 18)
end_date = date(2020, 9, 29)

#Select destination file
option = 2
file = "F:/Portugal/Hydro_Portugal_Files/"
files_download = ["C:/Users/ecampillos/Downloads/RES_Excel.xls", "C:/Users/ecampillos/Downloads/BH_ALB_Excel.xls", "C:/Users/ecampillos/Downloads/INT_Excel.xls"]
file_destiny = ["C:/Users/ecampillos/Downloads/balance_files/", "C:/Users/ecampillos/Downloads/hydro_files/", "C:/Users/ecampillos/Downloads/import_export_files/"]
URL_Balance = "https://www.centrodeinformacao.ren.pt/EN/InformacaoExploracao/Pages/EstatisticaDiaria.aspx"
URL_Hydro =  "https://www.centrodeinformacao.ren.pt/EN/InformacaoExploracao/Pages/EstatisticaDiariaHidraulica.aspx"
URL_imp_exp = "https://www.centrodeinformacao.ren.pt/EN/InformacaoExploracao/Pages/EstatisticaDiariaInterligacoes.aspx"
urls = [URL_Balance, URL_Hydro, URL_imp_exp]

year = ["ctl00$m$g_5e80321e_76aa_4894_8c09_4e392fc3dc7d$txtDatePicker$foo", "ctl00$m$g_054f8973_93d1_4015_9486_39fecd6994bb$txtDatePicker$foo", "ctl00$m$g_e29bc869_4e5b_4d90_a1e2_21fcaf7ba379$ddlAnos$foo"]
execute_b = ['//*[@id="ctl00_m_g_5e80321e_76aa_4894_8c09_4e392fc3dc7d_cmdCxecutar"]', '//*[@id="ctl00_m_g_054f8973_93d1_4015_9486_39fecd6994bb_cmdCxecutar"]', '//*[@id="ctl00_m_g_e29bc869_4e5b_4d90_a1e2_21fcaf7ba379_cmdCxecutar"]']
download_b = ['//*[@id="ctl00_m_g_5e80321e_76aa_4894_8c09_4e392fc3dc7d_btnExcel"]','//*[@id="ctl00_m_g_054f8973_93d1_4015_9486_39fecd6994bb_btnExcel"]', '//*[@id="ctl00_m_g_e29bc869_4e5b_4d90_a1e2_21fcaf7ba379_btnExcel"]' ]

for single_date in daterange(start_date, end_date):
    driver = webdriver.Chrome(file + "chromedriver.exe")
    url = urls[option]
    driver.get(url)
    date = single_date.strftime("%d-%m-%Y")
    elem = driver.find_element_by_name(year[option])
    elem.clear()
    elem.send_keys(date)
    elem.send_keys(Keys.RETURN)
    assert "No results found." not in driver.page_source

    # click submit button
    submit_button = driver.find_elements_by_xpath(execute_b[option])[0]
    submit_button.send_keys(Keys.RETURN)
    # click download button
    submit_button = driver.find_elements_by_xpath(download_b[option])[0]
    submit_button.click()
    driver.close()
    segs = 0
    while not os.path.exists(files_download[option]):
        time.sleep(1)
        segs += 1
    if os.path.isfile(files_download[option]):
        os.rename(files_download[option], file_destiny[option] +  single_date.strftime("%Y-%m-%d") + ".xls")
        print(str(int((single_date - start_date).days)) + "/" + str(int((end_date - start_date).days)) + " : " + str(segs) + " segs")

    else:
        raise ValueError("%s isn't a file!" % file_path)

    
